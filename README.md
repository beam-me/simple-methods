# SIMPLE-PIPS

The SIMPLE-methods model was designed for the experimenal application of decomposition approaches on energy system 
optimization models. They are intended to provide some additional help to modellers and practival examples alongside
the [best practice guide](https://gitlab.com/beam-me/bpg/-/blob/master/EnergySystemModel_SpeedUp_BestPracticGuide.pdf).

1. [The SIMPLE model](#the-simple-model)
    1. [Standard LP](#standard-lp)
	2. Decomposition approaches for deterministic problems
		1. [Rolling horizon](#rolling-horizon)
		2. [Benders decomposition](#benders-decomposition)
		2. [Lagrange relaxation](#lagrange-relaxation)
	3. Decomposition approaches for stochastic problems
		1. [Deterministic equivalent](#deterministic-equivalent)
		2. [Extended Mathematical Programming Framework](#extended-mathematical-programming-framework)
		3. [Benders decomposition (sequential)](#benders-decomposition-sequential)
		4. [Benders decomposition (asynchronous)](#benders-decomposition-asynchronous)
		5. [Benders decomposition (parallel via MPI)](#benders-decomposition-parallel-via-mpi)
2. [Authors](#authors)
3. [Acknowledgements](#acknowledgements)


## The SIMPLE model

The SIMPLE model is a highly simplified representation of an energy system optimization model (ESOM) developed during
the BEAM-ME project. The model adressess the need to have a common ground to discuss speed-up methods for complex
ESMs. This is achieved by preserving relevant parts of the model structure that can be found in many complex ESMs 
but at the same time having a compact and comprehensive source code.

All SIMPLE models come with a data generator simple\_data\_ gen.gms that can be parametrized to generate data instances
of different size. The data generator is called automatically from the main model and takes only the number of regions
as an argument to scale the problem size. All the data is computed by randomizing standard basic time series that 
provide the corresponding data for a model region.

Data sources:
- **timeSeries.csv**
  - demand: [Actual electricity consumption in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:2,%22selectedSubCategory%22:5,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - photovoltaic: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - wind: [Actual electricity generation in Germany 2019](https://www.smard.de/en/downloadcenter/download_market_data/5730#!?downloadAttributes=%7B%22selectedCategory%22:1,%22selectedSubCategory%22:1,%22selectedRegion%22:%22DE%22,%22from%22:1546297200000,%22to%22:1577832359999,%22selectedFileType%22:%22CSV%22%7D) 
    by [Bundesnetzagentur | SMARD.de](https://www.smard.de/) licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/)
  - ecars: Mathematical approximation of a daily profile f(t) = 0.3 * sin( (t - 55) / 17.3 ) + 0.2 * sin( (t - 30.2) / 7 ) + 0.48

All other data are rough estimations based on the general range of technoeconomical data found in energy system models.

For a comprehensive explanation of all SIMPLE models and corresponding options head over to the 
[best practice guide](https://gitlab.com/beam-me/bpg/-/blob/master/EnergySystemModel_SpeedUp_BestPracticGuide.pdf).

### Standard LP

Assuming that GAMS has been properly installed, the SIMPLE models can be run from the command line as follows:

```bash
gams simple.gms [optinal parameters]
```

| Parameter  | Description                                               | Default     |
| ---------- | --------------------------------------------------------- | ----------- |
| FROM       | Start of the time horizon (0 corresponds to hour 1)       | 0           |
| TO         | End of the time horizon (1 corresponds to hour 8760)      | 1           |
| RESOLUTION | Resolution of the model (1 corresponds to a hourly model) | 1           |
| NBREGIONS  | Number of model regions to be considered                  | 4           |
| METHOD     | Decomposition approach used to solve the model            | standard_lp |


## Decomposition methods for the deterministic problem

### Rolling horizon

```bash
gams simple.gms --METHOD=rolling_horizon
```


### Benders decomposition

```bash
gams simple.gms --METHOD=benders
```


### Lagrange relaxation

```bash
gams simple.gms --METHOD=lagrange_relaxation
```



## Decomposition methods for the stochastic problem


### Deterministic equivalent

```bash
gams simple.gms --METHOD=spExplicitDE
```


### Extended Mathematical Programming Framework

```bash
gams simple.gms --METHOD=stochasticEMP
```



### Benders decomposition (sequential)

```bash
gams simple.gms --METHOD=spBendersSeq
```



### Benders decomposition (asynchronous)

```bash
gams simple.gms --METHOD=spBendersAsync
```



### Benders decomposition (parallel via MPI)

This version requires a software setup with an MPI installation.

```bash
srun gams simple.gms --METHOD=spBendersMPI fileStem=SPBENDERS_MPI_ fileStemApFromEnv=PMI_RANK
```


## Authors
- Manuel Wetzel (German Aerospace Center)
- Frederik Fiand (GAMS Software GmbH)
- Michael Bussieck (GAMS Software GmbH)


## Acknowledgements
This research is part of the BEAM-ME project. It was funded by the German Federal Ministry for Economic Affairs and 
Energy under grant number FKZ 03ET4023A.
